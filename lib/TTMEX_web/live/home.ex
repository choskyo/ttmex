defmodule TTMEXWeb.HomeLive do
  use Phoenix.LiveView

  @topic "chat"

  def mount(_params, _session, socket) do
    TTMEXWeb.Endpoint.subscribe(@topic)

    baseState = [
      %{user: "server", time: DateTime.utc_now(), content: "connected"}
    ]

    {:ok, assign(socket, :messages, baseState)}
  end

  def handle_info(%{topic: @topic, payload: new_message}, socket) do
    {:noreply, assign(socket, messages: [new_message | socket.assigns.messages])}
  end

  def handle_info(update, socket) do
    newlist = [update.new_message | socket.assigns.messages]

    TTMEXWeb.Endpoint.broadcast_from(self(), @topic, "new_message", update.new_message)

    {:noreply, assign(socket, messages: newlist)}
  end

  def render(assigns) do
    ~L"""
    <div>
    <%= live_component @socket, TTMEXWeb.InputLive, id: :inp, messages: assigns.messages %>
        <ul>
          <%= for msg <- @messages do %>
            <%= TTMEXWeb.Message.render(msg) %>
          <% end %>
        </ul>
      </div>
    """
  end
end
