defmodule TTMEXWeb.InputLive do
  use Phoenix.LiveComponent
  use Phoenix.HTML

  @impl true
  def mount(socket) do
    username = "User" <> to_string(:rand.uniform(999))

    {:ok,
     assign(socket,
       username: username,
       m: %{
         username: username,
         message: ""
       }
     )}
  end

  @impl true
  def handle_event("post", params, socket) do
    model = Map.get(params, "m")

    new_message = %{
      user: socket.assigns.username,
      time: DateTime.utc_now(),
      content: Map.get(model, "message")
    }

    send(self(), %{new_message: new_message})

    assign(
      socket,
      m: %{
        username: socket.assigns.username,
        message: ""
      }
    )

    {:noreply, socket}
  end
end
