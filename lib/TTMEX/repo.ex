defmodule TTMEX.Repo do
  use Ecto.Repo,
    otp_app: :TTMEX,
    adapter: Ecto.Adapters.Postgres
end
